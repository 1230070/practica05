package upv.com.practica5;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View.OnClickListener;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_apple: openApple();
                return true;
            case R.id.action_grape: openGrape();
                return true;
            case R.id.action_pineapple: openPineapple();
                return true;
            case R.id.action_watermelon: openWatermelon();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*public void openUp(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }*/

    public void openApple(){
        Intent intent = new Intent(this, MainApple.class);
        startActivity(intent);
    }

    public void openGrape(){
        Intent intent = new Intent(this, MainGrape.class);
        startActivity(intent);
    }

    public void openPineapple(){
        Intent intent = new Intent(this, MainPineapple.class);
        startActivity(intent);
    }

    public void openWatermelon(){
        Intent intent = new Intent(this, MainWatermelon.class);
        startActivity(intent);
    }
}
